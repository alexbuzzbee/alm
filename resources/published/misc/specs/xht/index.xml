<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/css" href="https://purl.org/extensible_hypertext/resources/xht_polyfill_structure.css"?>
<?xml-stylesheet type="text/css" href="https://purl.org/extensible_hypertext/resources/xht_polyfill_text.css"?>
<?xml-stylesheet type="text/css" href="https://purl.org/extensible_hypertext/resources/xht_polyfill_meta.css"?>
<?xml-stylesheet type="text/css" href="https://purl.org/extensible_hypertext/resources/xht_polyfill_linking.css"?>
<?xml-stylesheet type="text/xsl" href="https://purl.org/extensible_hypertext/resources/xht_polyfill_linking.xsl"?>
<hs:document xmlns:hs="http://purl.org/extensible_hypertext/structure" xmlns:hm="http://purl.org/extensible_hypertext/meta" xmlns:ht="http://purl.org/extensible_hypertext/text" xmlns:hl="http://purl.org/extensible_hypertext/linking" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/terms">
  <hm:tag property="http://purl.org/dc/terms/title">Extensible Hypertext standard</hm:tag>
  <hm:tag property="http://purl.org/dc/terms/creator" href="https://www.alm.website/">Alexander L. Martin</hm:tag>
  <hm:tag property="http://purl.org/dc/terms/date">2019-08-21</hm:tag>
  <ht:para>
    Extensible Hypertext (XHT) is a modular XML format for hypertext documents, inspired by XHTML2 but intended to make a clean break from HTML and JavaScript and achieve superior semantic value.
  </ht:para>
  <ht:para>
    This document does not define the entirety of XHT; rather, it defines the overall common features of XHT modules. The module specifications may be <hl:link xlink:href="modules">found separately</hl:link>.
  </ht:para>
  <ht:para>
    The key words "must", "must not", "required", "shall", "shall not", "should", "should not", "recommended", "not recommended", "may", and "optional" in this document are to be interpreted as described in <hl:link xlink:href="urn:ietf:bcp:14">BCP 14</hl:link>.
  </ht:para>
  <ht:para>
    Note: These documents are currently request-for-comments drafts. They must not be considered final; any aspect of the XHT standards may be changed during the request-for-comments period. Implementors are strongly advised to wait for the end of this period before releasing the results of any implementation work.
  </ht:para>
  <hs:section xml:id="modularity">
    <hm:tag property="http://purl.org/dc/terms/title">Modularity</hm:tag>
    <ht:para>
      XHT is a modular standard. This means that it is broken into a number of separate modules, which can be combined to provide a flexible set of tools for document authoring. XHT's modularity provides a far greater level of flexibility and forwards compatibility.
    </ht:para>
    <ht:para>
      Each XHT module shall be assigned an XML Namespace; in the case of official modules, under <hl:link xlink:href="http://purl.org/extensible_hypertext">the XHT root URI</hl:link>. Using separate namespaces increases verbosity, but allows authors to choose which modules they use and XHT-native user agents to choose which modules they support.
    </ht:para>
  </hs:section>
  <hs:section xml:id="compatibility">
    <hm:tag property="http://purl.org/dc/terms/title">Compatibility</hm:tag>
    <ht:para>
      As far as possible, XHT modules should be backwards-compatible with non-XHT-native user agents, such as Web browsers, through the use of "polyfill" XSLT and CSS stylesheets. The URIs of these stylesheets shall contain the magic string "xht_polyfill_" followed by the name of the polyfilled module.
    </ht:para>
    <ht:para>
      XHT-native user agents should ignore polyfill stylesheets for modules they understand, and may ignore other polyfill stylesheets. XHT documents should explicitly reference the appropriate polyfill stylesheets.
    </ht:para>
    <ht:para>
      XHT documents may be served over any protocol able to support simple request-response resource retrieval by URI; they are not tied to HTTP or any other protocol.
    </ht:para>
  </hs:section>
  <hs:section xml:id="reuse">
    <hm:tag property="http://purl.org/dc/terms/title">Reuse of existing technology</hm:tag>
    <ht:para>
      XHT modules should reuse existing technologies, such as XML itself, RDF, XLink, XML stylesheets, and so on wherever reasonable. This reduces the work needed to both develop and implement a module. Where existing technologies are deemed overly-complicated, XHT modules should specify recommended subsets that provide the essential functionality.
    </ht:para>
  </hs:section>
  <hs:section xml:id="profiles">
    <hm:tag property="http://purl.org/dc/terms/title">Common profiles of XHT</hm:tag>
    <ht:para>
      While XHT does not specify a "standard set" of modules, it does define several "profiles," which are sets of modules that fit common use cases or capability levels.
    </ht:para>
    <hs:section xml:id="minimal-profile">
      <hm:tag property="http://purl.org/dc/terms/title">Minimal profile</hm:tag>
      <ht:para>
        The minimal profile contains the smallest subset of XHT modules viable for basic structured document authoring. It consists of the <hl:link xlink:href="modules/structure">Structure</hl:link> module and the <hl:link xlink:href="modules/text">Text</hl:link> module. Notably absent are the Meta module and the Linking module. The minimal profile is not recommended for general use due to its limited capabilities.
      </ht:para>
    </hs:section>
    <hs:section xml:id="basic-profile">
      <hm:tag property="http://purl.org/dc/terms/title">Basic profile</hm:tag>
      <ht:para>
        The basic profile contains a subset of XHT modules suitable for basic general-purpose hypertext authoring. It consists of the <hl:link xlink:href="modules/structure">Structure</hl:link> module, the <hl:link xlink:href="modules/text">Text</hl:link>  module, the <hl:link xlink:href="modules/meta">Meta</hl:link> module, and the <hl:link xlink:href="modules/linking">Linking</hl:link> module.
      </ht:para>
    </hs:section>
  </hs:section>
</hs:document>
