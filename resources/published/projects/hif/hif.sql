-- HERO Interchange Format schema, version 1, draft 1
-- HIF is a SQLite 3-based format. This file defines the format; it contains the SQL schema and comments explaining it. You can think of it as similar to a Document Type Definition or XML Schema for an XML-based format.

-- HIF is a format designed to represent character sheets for the 6th Edition of the HERO System. An HIF file is a container that can store from a single sheet to an entire campaign's library of PC, NPCs of all kinds, vehicles, bases, and so on. HIF is also designed to store the additional information needed by combat tracking tools, e.g. damage, END used, Drains, Aids. A useful companion to HIF would be a format for storing all the definitions of things (both information for players/GMs and machine-readable information like a Power's effect scaling relative to its Active Points), and (as a stretch goal) a database containing 6E1.

-- You should always run these connection initialization statements after opening an HIF file.
PRAGMA foreign_keys=1; -- Enforce foreign keys.
PRAGMA recursive_triggers=1; -- Allow recursive triggers, in case they are added to the format some day.
PRAGMA trusted_schema=1; -- Reduce attack surface exposed to a malicious HIF file.
-- End connection initialization.

-- Database configuration.
PRAGMA encoding='UTF-8';
PRAGMA application_id=0x4845524f; -- 'HERO'.
PRAGMA user_version=101; -- Numbering scheme: Top-level version followed by two-digit draft number. Draft 00 is final. So 101 < 102 < 150 < 199 < 100 < 201 < 200.

CREATE TABLE campaign ( -- The "Campaign Information" block on the back of a 6E1 character sheet.
  campaign_name TEXT PRIMARY KEY,
  edition INTEGER DEFAULT 6 CHECK(edition = 6), -- Version 1 of HIF only supports 6th Edition. This will probably always be true, but if it changes, this field will become useful to determine which rules apply and which columns and tables are used.
  genre TEXT,
  gamemaster TEXT
) STRICT, WITHOUT ROWID; -- All tables will be STRICT, most will be WITHOUT ROWID.

CREATE TABLE sheet ( -- A sheet. Could be a character, base, vehicle, etc. This table can be extended.
  sheet_id INTEGER PRIMARY KEY, -- As a ROWID alias, SQLite will auto-generate a sheet ID.
  campaign_name TEXT NOT NULL REFERENCES campaign(campaign_name) ON UPDATE CASCADE ON DELETE RESTRICT, -- Same column name for NATURAL JOIN.
  type TEXT NOT NULL, -- e.g. 'character', 'vehicle', 'base'
  name TEXT NOT NULL,
  player TEXT,
  alternate_identities TEXT, -- Conventionally comma-space-separated names. For characters.
  height REAL, -- Centimeters. For characters.
  weight REAL, -- Kilograms. For characters.
  hair_color TEXT, -- For characters.
  eye_color TEXT, -- For characters.
  location TEXT, -- For bases.
  grounds TEXT, -- For bases.
  -- Note that for bases and vehicles, size is a characteristic.
  appearance TEXT,
  base_points INTEGER, -- Character Points not requiring matching complications.
  max_complications INTEGER, -- Maximum points of matching complications.
  total_complications INTEGER, -- Total points of complications (can be more than max_complications, but then only max_complications points are available).
  total_experience INTEGER, -- Total points of XP received.
  experience_spent INTEGER, -- Points of XP spent.
  experience_unspent INTEGER GENERATED ALWAYS AS (total_experience - experience_spent) VIRTUAL, -- Points of XP not yet spent.
  max_points INTEGER GENERATED ALWAYS AS (MAX(max_complications, total_complications) + base_points + total_experience) VIRTUAL, -- Maximum spendable points.
  points_spent INTEGER, -- All points spent, counting XP.
  notes TEXT
  -- Additional columns can be created with ALTER TABLE ADD COLUMN to represent other attributes. Use PRAGMA table_info to test if these columns are present, and be sure to access them by name, not by position. It might be advisable to create additional columns as ANY or TEXT unless a numeric format is very obvious, because others might come up with the same additional columns as you.
) STRICT;

CREATE TABLE attachment ( -- An extra chunk of data associated with a sheet. Many sheets may have an attachment called 'Image', probably 'image/png' or 'image/jpeg'.
  sheet_id INTEGER REFERENCES sheet(sheet_id) ON UPDATE CASCADE ON DELETE CASCADE,
  name TEXT NOT NULL,
  media_type TEXT DEFAULT 'application/octet-stream',
  data BLOB,
  PRIMARY KEY (sheet_id, name)
) STRICT, WITHOUT ROWID;

CREATE TABLE characteristic ( -- A characteristic value on a sheet, including defenses.
  sheet_id INTEGER NOT NULL REFERENCES sheet(sheet_id) ON UPDATE CASCADE ON DELETE CASCADE,
  short_name TEXT NOT NULL, -- e.g. CON, BODY, PD, rPD.
  long_name TEXT, -- e.g. Constitution, Body, Physical Defense, Resistant Physical Defense.
  value INTEGER NOT NULL,
  adjustment INTEGER DEFAULT 0, -- Amount by which value has been Adjusted. Positive for gain, negative for loss. The roll column should not be updated in the database. A combat tracking tool should recalculate the roll itself, in memory.
  used INTEGER DEFAULT 0, -- Amount lost to damage, consumption, etc.
  current_value INTEGER GENERATED ALWAYS AS (value + adjustment - used) VIRTUAL, -- Value after Adjustment and usage.
  roll INTEGER, -- Or less on 3d6. Not applicable to all characteristics.
  notes TEXT,
  active_points INTEGER NOT NULL, -- Active Points in the characteristic.
  real_points INTEGER NOT NULL, -- Cost that should be paid for the characteristic by the rules. Negative when sold back.
  override_cost INTEGER, -- Different cost dictated by the GM. Normally NULL.
  sourcebook TEXT DEFAULT '6E1', -- Citation.
  sourcebook_page TEXT,
  PRIMARY KEY (sheet_id, short_name)
), STRICT, WITHOUT ROWID;

CREATE TABLE wound

CREATE TABLE purchase ( -- Things that points are paid for with relatively straightforward cost structures. Skills, Perks, Talents, Combat Skill Levels, Senses, etc.
  sheet_id INTEGER NOT NULL REFERENCES sheet(sheet_id) ON UPDATE CASCADE ON DELETE CASCADE,
  type TEXT NOT NULL, -- e.g. 'skill', 'perk', 'talent', 'combat_skill_level'.
  name TEXT NOT NULL,
  levels INTEGER DEFAULT 1, -- Level 0 of a Skill is not having it at all. Level 1 is familiarity. Level 2 is proficiency. Level 3 is a basic purchase. Additional levels represent purchasing a larger bonus to the roll (level 4 is +1, etc.), which is the same as buying Skill Levels for a single Skill. For multi-purpose Skill Levels, this column is just the number of levels bought.
  roll INTEGER, -- Or less on 3d6. Not applicable to all purchases.
  notes TEXT,
  active_points INTEGER NOT NULL, -- Active Points in the purchase.
  adjustment INTEGER NOT NULL, -- Amount by which Active Points have been Adjusted. Positive for gain, negative for loss. The levels and roll column should not be updated in the database. A combat tracking tool should recalculate the levels and roll itself, in memory, or simply indicate the Adjustment so it can be applied manually.
  current_active_points INTEGER GENERATED ALWAYS AS (active_points + adjustment) VIRTUAL,
  real_points INTEGER NOT NULL, -- Cost that should be paid for the purchase by the rules. Negative when e.g. selling back Everyman skills.
  override_cost INTEGER, -- Different cost dictated by the GM. Normally NULL.
  sourcebook TEXT DEFAULT '6E1', -- Citation.
  sourcebook_page TEXT,
  PRIMARY KEY (sheet_id, name)
) STRICT, WITHOUT ROWID;

CREATE TABLE maneuver ( -- Attacks and Maneuvers block.
  sheet_id INTEGER NOT NULL REFERENCES sheet(sheet_id) ON UPDATE CASCADE ON DELETE CASCADE,
  name TEXT NOT NULL,
  phase TEXT NOT NULL,
  ocv INTEGER NOT NULL,
  dcv INTEGER NOT NULL,
  effect TEXT NOT NULL,
  notes TEXT,
  PRIMARY KEY (sheet_id, name)
) STRICT, WITHOUT ROWID;

CREATE TABLE hit_location ( -- Hit Locations chart.
  sheet_id INTEGER NOT NULL REFERENCES sheet(sheet_id) ON UPDATE CASCADE ON DELETE CASCADE,
  name TEXT NOT NULL,
  min_roll INTEGER, -- Lowest number on 3d6 hitting here.
  max_roll INTEGER, -- Highest number on 3d6 hitting here.
  stun_multiplier REAL,
  body_multiplier REAL,
  to_hit_modifier INTEGER, -- When trying to hit here with a placed attack.
  pd INTEGER,
  ed INTEGER,
  weight INTEGER, -- Kilograms.
  notes TEXT,
  PRIMARY KEY (sheet_id, name)
) STRICT, WITHOUT ROWID;

CREATE TABLE complication ( -- A Complication.
  sheet_id INTEGER NOT NULL REFERENCES sheet(sheet_id) ON UPDATE CASCADE ON DELETE CASCADE,
  name TEXT NOT NULL,
  notes TEXT,
  value INTEGER NOT NULL, -- Points the Complication should be worth by the rules.
  override_value INTEGER, -- Different value dictated by the GM. Normally NULL.
  sourcebook TEXT DEFAULT '6E1', -- Citation.
  sourcebook_page TEXT,
  PRIMARY KEY (sheet_id, name)
) STRICT, WITHOUT ROWID;

CREATE TABLE power ( -- Core information about a Power, including movement and naked Advantages. Modifiers and adders in separate tables.
  power_id INTEGER PRIMARY KEY, -- Using a ROWID alias makes foreign keys easier.
  sheet_id INTEGER NOT NULL REFERENCES sheet(sheet_id) ON UPDATE CASCADE ON DELETE CASCADE,
  framework
  name TEXT NOT NULL, -- e.g. Lightning Bolt, Kevlar Vest, Breathing Mask, Flash Grenade.
  base_power TEXT NOT NULL, -- e.g. Blast, Resistant Protection, Life Support, Flash.
  effect TEXT, -- e.g. '6d6', '10 points', 'Self-Contained Breathing', 'Sight Group'.
  notes TEXT,
  enabled INTEGER CHECK(enabled IN (0, 1)) DEFAULT 0, -- Whether the power is currently available. Setting to zero is like crossing out the power.
  free INTEGER CHECK(free IN (0, 1)), -- Whether the power is free (usually because it's equipment in a Heroic campaign).
  active_points INTEGER NOT NULL, -- Active Points in the power.
  adjustment INTEGER NOT NULL, -- Amount by which Active Points have been Adjusted. Positive for gain, negative for loss. The effect column should not be updated in the database. A combat tracking tool should recalculate the effect itself, in memory, or simply indicate the Adjustment so it can be applied manually.
  current_active_points INTEGER GENERATED ALWAYS AS (active_points + adjustment) VIRTUAL,
  real_points INTEGER NOT NULL, -- Cost that should be paid for the power by the rules. Negative when e.g. selling back Running.
  override_cost INTEGER, -- Different cost dictated by the GM. Normally NULL.
  sourcebook TEXT DEFAULT '6E1', -- Citation.
  sourcebook_page TEXT
) STRICT;

CREATE TABLE adder ( -- A Power Adder.
  power_id INTEGER REFERENCES power(power_id) ON UPDATE CASCADE ON DELETE CASCADE,
  name TEXT NOT NULL, -- e.g. Damage Negation.
  levels INTEGER DEFAULT 1,
  adjustment INTEGER, -- Number of levels that have been Adjusted. Positive for gain, negative for loss.
  current_levels INTEGER GENERATED ALWAYS AS (levels + adjustment) VIRTUAL,
  cost INTEGER NOT NULL,
  notes TEXT,
  PRIMARY KEY (power_id, name)
) STRICT, WITHOUT ROWID;

CREATE TABLE modifier ( -- A Power Advantage or Limitation.
  power_id INTEGER REFERENCES power(power_id) ON UPDATE CASCADE ON DELETE CASCADE,
  type TEXT CHECK(type IN ('Advantage', 'Limitation')),
  name TEXT,
  notes TEXT,
  magnitude REAL, -- e.g. 0.5, -0.25.
  applies_to_points
  sourcebook TEXT DEFAULT '6E1', -- Citation.
  sourcebook_page TEXT,
  PRIMARY KEY (power_id, name)
) STRICT, WITHOUT ROWID;
