<?xml version="1.0" encoding="UTF-8"?>
<!-- XSLT polyfill to make XHT links work in Web browsers without XLink support. -->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:hl="http://purl.org/extensible_hypertext/linking" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0">
  <xsl:output method="xml" encoding="UTF-8" indent="yes" />
  <xsl:template match="hl:link">
    <html:a style="display: inline;">
      <xsl:attribute name="href">
        <xsl:value-of select="@xlink:href" />
      </xsl:attribute>
      <xsl:value-of select="." />
    </html:a>
  </xsl:template>
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" />
    </xsl:copy>
  </xsl:template>
</xsl:transform>
