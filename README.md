# alm

This is my personal Web site, [`www.alm.website`](https://www.alm.website/). It uses the [XSite static site generator](https://gitlab.com/alexbuzzbee/xsite).

## Hosting

I keep this site on a machine called `hestia`, which also acts as my personal file server and hosts Git repository origins. The Apache configuration is in `httpd-config/001-alm.conf`. An installation of [Caddy](https://caddyserver.com/) acts as a reverse proxy so that I can run other Web applications, and it automatically issues and renews TLS certificates. For the moment, DigitalOcean's hosted DNS provides name resolution, though I'd like to change that.

## Build and deployment

The `scripts` directory holds the scripts that control building and deploying the site. This is a complicated dance involving a triangular Git topology (my desktop pushes to a repository in my home directory on `hestia`, which then pushes to `origin`, from which my desktop pulls) and `rsync`.

## Contents

The "framework" of the site consists of `templates/`, `documents/includes/`, `documents/error/`, `resources/published/static/css`, `resources/published/static/js`, `httpd-config/`, `config.ini`, and `scripts`, and includes:

* The navbar.
* The `main` and `blog` templates.
* The CSS for the navbar and the site as a whole.
* The error pages, including the Internet Explorer warning.
* The build and deployment scripts.

## License concerns

The contents of `templates/`, `documents/includes/`, `documents/error/`, `resources/published/static/css`, `resources/published/static/js`, `httpd-config/`, and `config.ini` are licensed as follows:

> Copyright 2019, 2021, 2022 Alex L. Martin

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

The contents of `resources/published/auth/` are public domain under Creative Commons Zero, as dedicated by the original author.

All other content remains fully copyrighted. To facilitate using it as an example and learning exercise for XHTML and XSite, I grant permission to copy these fully-copyrighted portions to your own computer and between locations and programs on your own computer, study it, make changes, and derive other work from it. All other rights are reserved. Please note in particular that republishing the content of this site (everything not otherwise excepted), or direct derivatives of it, without my separate permission is expressly *not* allowed and is therefore illegal in many countries under copyright law. I don't expect to enforce this, but I would consider it very rude to republish my site without asking.

In no event shall the above or any part of the above be considered an endorsement of anything derived from this software by parties other than the author.
