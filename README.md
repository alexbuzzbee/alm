# alm

This is my personal Web site, [`www.alm.website`](https://www.alm.website/). It uses the [XSite static site generator](https://gitlab.com/alexbuzzbee/xsite).

## Hosting

The site is hosted on a Debian DigitalOcean Droplet using Apache. The Apache VirtualHost configuration used is in `httpd-config/001-alm.conf`. EFF CertBot automatically maintains a Let's Encrypt certificate, and DNS is handled by DigitalOcean.

## Build and deployment

A directory in my home directory on the server contains Git checkouts of both the site and XSite. When I want to publish a new version of the site, I run a script that pulls both of these repositories, installs XSite's dependencies with `pip`, and runs XSite. If I'm satisfied with the output, I manually copy it to the Apache VirtualHost directory.

## Contents

The "framework" of the site consists of `templates/`, `documents/includes/`, `documents/error/`, `resources/published/static/`, `httpd-config/`, `config.ini` and includes:

* The navbar.
* The `main` and `blog` templates.
* The CSS for the navbar and the site as a whole.
* The error pages, including the Internet Explorer warning.

## License concerns

The contents of `templates/`, `documents/includes/`, `documents/error/`, `resources/published/static/`, `httpd-config/`, and `config.ini` are licensed as follows:

> Copyright 2019, 2021 Alex L. Martin

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

The contents of `resources/published/auth/` are public domain under Creative Commons Zero, as dedicated by the original author.

All other content remains fully copyrighted. To facilitate using it as an example and learning exercise for XHTML and XSite, I grant permission to copy these fully-copyrighted portions to your own computer and study it, make changes, and derive other work from it. All other rights are reserved. Please note in particular that republishing the content of this site (the contents of `documents/index.xml`, `documents/about.xml`, `documents/blog/`, `documents/misc/`, `documents/opinions/`, and `resources/published/misc/`), or direct derivatives of it, without my separate permission is expressly *not* allowed and is therefore illegal in many countries under copyright law. I don't expect to enforce this, but I would consider it very rude to republish my website without asking.

In no event shall the above or any part of the above be considered an endorsement of anything derived from this software by parties other than the author.
