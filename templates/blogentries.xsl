<?xml version="1.0" encoding="UTF-8"?>
<?xsite-template name="main.xml"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="dataset">
    <section>
      <h3>Entries</h3>
      <ul>
        <xsl:apply-templates />
      </ul>
    </section>
  </xsl:template>
  <xsl:template match="record">
    <li><em><xsl:value-of select="@date" /></em> - <a><xsl:attribute name="href"><xsl:value-of select="@path" /></xsl:attribute><xsl:value-of select="@title" /></a></li>
  </xsl:template>
</xsl:stylesheet>
