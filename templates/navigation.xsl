<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="path" />
  <xsl:template match="dataset">
    <nav>
      <ul>
        <xsl:apply-templates />
      </ul>
      <xsl:choose>
        <xsl:when test="$path='/'">
          <style>nav {
    speak: always;
}</style>
        </xsl:when>
        <xsl:otherwise>
          <style>nav a[href='/'] {
    speak: always;
}</style>
        </xsl:otherwise>
      </xsl:choose>
    </nav>
  </xsl:template>
  <xsl:template match="record">
    <xsl:choose>
      <xsl:when test="@path=$path">
        <li><a href="#" aria-current="page"><strong><xsl:value-of select="@name" /></strong></a></li>
      </xsl:when>
      <xsl:otherwise>
        <li><a><xsl:attribute name="href"><xsl:value-of select="@path" /></xsl:attribute><xsl:value-of select="@name" /></a></li>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
