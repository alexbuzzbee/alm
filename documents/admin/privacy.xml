<?xml version="1.0" encoding="UTF-8"?>
<?xsite-params title="Privacy policy" path="/policy/privacy"?>
<main xmlns="http://www.w3.org/1999/xhtml">
  <h1>Privacy policy</h1>
  <p>To summarize: I do not collect your personal information on purpose. If you read my Web site or deliver email to me, my server will collect the IP address of the machine that connects to it to do that, and will store it in server logs with a timestamp and information about what was done, what software you used, and how successful you were. These logs are kept indefinitely for troubleshooting purposes. I may make them more transient in the future, but I haven't yet done so. No automated analysis is done. The only other information collected is that which you explicitly send me, such as if you submit a form, upload a file, or send an email. This information also will be kept indefinitely. If you want any information removed, contact me, and be prepared to specifically identify the information you want removed and prove that you are who you claim to be.</p>
  <section>
    <h1>Scope</h1>
    <p>This privacy policy applies to all services and functions provided intentionally to the public via the Internet falling into the domain <code>alm.website</code>. Specifically, the following services:</p>
    <ul>
      <li>The World Wide Web site <code>www.alm.website</code>, used to read static World Wide Web content authored by the owner.</li>
<!--       <li>The World Wide Web application <code>fossil.alm.website</code>, used to retrieve software source code authored by the owner.</li> -->
      <li>The Simple Mail Transfer Protocol (SMTP) server <code>mail.alm.website</code>, used to deliver e-mail to addresses at <code>alm.website</code>.</li>
    </ul>
    <p>Any other services under <code>alm.website</code>, or any uses of those services not included in the above list, are <a href="use">not intended for use by the general public</a> and are not within the scope of this privacy policy.</p>
  </section>
  <section>
    <h1>Collection</h1>
    <section>
      <h1><code>www.alm.website</code><!-- and <code>fossil.alm.website</code>--></h1>
      <p>The following information may be collected each time a request is made of the <code>www.alm.website</code><!-- or <code>fossil.alm.website</code>--> service:</p>
      <ul>
        <li>The date and time of the request.</li>
        <li>The Internet Protocol (IP) address associated with the computer used to make the request at the time of the request, and, if it can be determined, the domain name associated with that address.</li>
        <li>The type of request made, such as retrieving content, attempting to submit new content, or collecting meta-information about content.</li>
        <li>The Uniform Resource Identifier ("URI", also known as a "Uniform Resource Locator," "URL," or "Web address") of the content accessed.</li>
        <li>The name and version claimed by the software making the request.</li>
        <li>The user name that the software making the request associates with it for HTTP Basic Authentication purposes, if any. The service does not request such a user name, so most software will normally not associate one.</li>
        <li>The amount of information transferred in either direction in the course of the request.</li>
        <li>The Uniform Resource Identifier ("URI", also known as a "Uniform Resource Locator," "URL," or "Web address") of the content that the software making the request claims was responsible for directing it or the user to the service, also known as the "referrer." Often, depending on the software used, this information is more detailed when the referrer content is itself part of the <code>www.alm.website</code><!-- or <code>fossil.alm.website</code>--> service. Some software will not provide it at all in some or all configurations.</li>
        <li>Whether the request was successful, and, if not, the reason why the service was unable to fulfill it.</li>
        <li>Information about any anomalies in processing the request, or in the request itself.</li>
      </ul>
      <p>No other information is collected by the service unless that information appears to have been explicitly provided, such as if a form is submitted, nor does the service permit third parties to collect information via it. In particular, neither cookies nor any Web analytics or tracking mechanisms are used to identify users or remember where they have visited. There is a particular warning page that places a cookie only upon a button being clicked to prevent the warning from being redisplayed; this is the only cookie used for any purpose, and its presence or absence in requests is not logged. Since, other than the referrer, the service does not track users by default, it does not respond to the Do Not Track setting available in some software.</p>
    </section>
    <!--<section>
      <h1><code>fossil.alm.website</code> only</h1>
      <p>The following information</p>
    </section>-->
    <section>
      <h1><code>mail.alm.website</code></h1>
      <p>The following information may be collected each time a request is made of the <code>mail.alm.website</code> service:</p>
      <ul>
        <li>The dates and times that the request began, delivered each message, and concluded.</li>
        <li>The Internet Protocol (IP) address associated with the computer used to make the request at the time of the request, and, if it can be determined, the domain name associated with that address.</li>
        <li>The time taken to receive and deliver each message.</li>
        <li>The number of SMTP commands of each type sent in the course of the request.</li>
        <li>Whether the request was successful, and, if not, the reason why the service was unable to fulfill it.</li>
        <li>Information about any anomalies in processing the request, or in the request itself.</li>
      </ul>
      <p>Additionally, the complete content of each delivered message, and all associated meta-information, will be collected so it can be delivered to the recipient.</p>
    </section>
  </section>
  <section>
    <h1>Retention and storage</h1>
    <p>Information collected about requests made of all services is stored in textual server logs on the server associated with the service in question for an indefinite period, unless requested to be removed. It is also stored in backups of that server, again for indefinite periods.</p>
    <p>E-mail messages delivered via <code>mail.alm.website</code> will be stored in mailbox files associated with the recipient or recipients on the server associated with that service for an indefinite period. They are also stored in backups of that server, again for indefinite periods.</p>
    <p>Other than delivered e-mail messages, <code>alm.website</code> services currently accept and store no other information that appears to have been explicitly provided. If they did, that information would be stored in databases or files on the server associated with the service in question for an indefinite period, unless requested to be removed, as well as in backups of that server for indefinite periods.</p>
  </section>
  <section>
    <h1>Usage</h1>
    <p>No analysis of information collected about requests made of services is performed in an automated fashion. That information may be examined manually to identify the cause of a problem with a service, to determine the extent to which a service is being used and for approximately what purposes, or to determine what information is present in order to execute this policy.</p>
    <p>Any information that appears to have been explicitly provided to a service by a user, such as content of form submissions, uploaded files, or e-mail messages, will be examined manually and/or automatically to determine its intended purpose and then may be used for any lawful purpose and by any lawful means, unless requested otherwise; in particular, where applicable, information will be used for the purposes described in association with the means of delivery, or within the information provided.</p>
  </section>
  <section>
    <h1>Disclosure to third parties</h1>
    <p>Information stored will not be disclosed to third parties unless required by United States or international laws. Unlawful demands will be contested to the extent practical.</p>
    <p>Except when entirely prohibited, a public announcement will be made when a compulsory or non-compulsory demand is received, contested, rejected, affirmed, or fulfilled containing what information can be published.</p>
  </section>
  <section>
    <h1>Data subject rights</h1>
    <p>Except as otherwise provided below, if a request to retrieve, remove, correct, or amend specific information is received from a requestor credibly claiming to be the subject of that information, it will be provided, or removed from, corrected, or amended in active systems. Since backups cannot be modified at a sufficiently granular level, information will only be removed from backup storage when the backups are deleted, which is performed irregularly but generally within one year of creation, and will be re-removed from, re-corrected, or re-amended in active systems if a backup is restored.</p>
    <p>The preferred means of contact for removal and correction requests is the e-mail address <a href="mailto:privacy12@alm.website"><code>privacy12@alm.website</code></a>. Other reasonable means of communication will also be accepted, but the right to request that a written request be submitted is reserved. Note that this email address <em>may change without notice</em> other than being updated here.</p>
    <section>
      <h1>Exceptions</h1>
      <p>Information will not be provided, corrected, or amended upon request if it appears not to be present within a service.</p>
      <p>Information will not be corrected or amended upon request if the correction or amendment is determined to be false.</p>
      <p>Information will not be removed, corrected, or amended upon request if the information is correct, not personally identifying, and published for reasons in the public interest.</p>
      <p>Information will not be removed or amended upon request if there is reason to believe it is evidence of unlawful activity.</p>
      <p>Information will not be removed, corrected, or amended upon request if doing so would run counter to any requirement of law or contract, or any other mandatory obligation.</p>
    </section>
  </section>
  <section>
    <h1>Updates</h1>
    <p>This policy will be updated as needed. It is the responsibility of the user of a service to inspect the privacy policy to their satisfaction. No information is retained about the identities of users, so no notifications can be sent of changes in the policy.</p>
  </section>
  <section>
    <h1>Questions, comments, and concerns</h1>
    <p>If you would like to ask a question regarding this privacy policy or other privacy concerns related to the <code>alm.website</code> domain, or if you have concerns or comments, please direct those communications to <a href="mailto:privacy12@alm.website"><code>privacy12@alm.website</code></a>. Note that this email address <em>may change without notice</em> other than being updated here.</p>
  </section>
</main>
