<?xsite-params title="Shooting yourself in the foot" keywords="Joke, Programming humor, Coding humor"?>
<main xmlns="http://www.w3.org/1999/xhtml">
  <section>
    <h2>Shooting yourself in the foot in various programming environments</h2>
    <p>This is inspired by, but not directly ripped off from, the original classic computer science joke of <a href="https://www-users.cs.york.ac.uk/susan/joke/foot.htm">shooting yourself in the foot in various programming languages</a>.</p>
    <p>Feel free to send me comments, suggestions, or criticism (see <a href="/">the home page</a> for contact methods).</p>
  </section>
  <section>
    <h3>C</h3>
    <p>You load the gun and take aim at your foot, but when you pull the trigger, you discover that one of the grains of gunpowder is a <code>NULL</code> pointer.</p>
    <code>Segmentation fault (core dumped)</code>
  </section>
  <section>
    <h3>C++</h3>
    <p>You shoot yourself in the foot, but forget to delete the bullet and leak memory.</p>
  </section>
  <section>
    <h3>Rust</h3>
    <p>The compiler won't let you take the safety off.</p>
  </section>
  <section>
    <h3>Assembly</h3>
    <p>You build your own gun and make your own bullets. When you pull the trigger, the gun fires forwards and backwards, jams, and falls apart.</p>
  <code>Segmentation fault (core dumped)</code> 
  </section>
  <section>
    <h3>Machine code</h3>
    <p>You hand-forge all the parts for a gun and bullet casings, mix your own gunpowder, and assemble it. When you pull the trigger, the world freezes.</p>
  </section>
  <section>
    <h3>Go</h3>
    <p>The standard library provides a gun and bullets, but your foot needs to implement interface <code>Shootable</code>.</p>
  </section>
  <section>
    <h3>Java</h3>
    <p>You use a <code>BulletFactory</code> to produce ammunition for one of several <code>Gun</code>s, wrap the whole thing in a <code>ShootingManager</code>, and fire at your foot, but the JVM hasn’t warmed up yet. Two minutes later, your foot throws a <code>PainException</code>.</p>
  </section>
  <section>
    <h3>.NET</h3>
    <p>You have a <code>System.Armaments.Firearms.Gun</code>, but can’t find any bullets anywhere. The documentation says they’re in the <code>Firearms</code> namespace with the gun, but doesn’t describe them.</p>
  </section>
  <section>
    <h3>Python</h3>
    <p>You shoot yourself in the foot with ease, but it takes long enough for you to get lunch and still come back in time to put your foot in front of the bullet.</p>
  </section>
  <section>
    <h3>BASIC</h3>
    <p>You dampen your foot and wait for it to rot.</p>
  </section>
  <section>
    <h3>JavaScript</h3>
    <p>There are eight frameworks for foot-shooting. Of these, three are unmaintained, two are brand-new and full of bugs, one lacks bullets, and the other two require jQuery.</p>
  </section>
  <section>
    <h3>Erlang</h3>
    <p>You take aim and pull the trigger, but the gun crashes because the bullets are of the wrong brand. Upon replacing the bullets and gun and trying again, a new foot, now with hole, appears from the gun. You do not know how to replace your existing foot with the shot one.</p>
  </section>
  <section>
    <h3>CSS</h3>
    <p>You fire and miss because your foot is where your knee should be.</p>
  </section>
  <section>
    <h3>HTML</h3>
    <p>You write a 15-page document describing how to shoot yourself in the foot.</p>
  </section>
  <section>
    <h3>TeX</h3>
    <p>You write a beautiful 5-page document describing how to shoot yourself in the foot. As a bonus, it does it for you when you compile it.</p>
  </section>
  <section>
    <h3>SQL</h3>
    <p>You miss, but you can roll it back and try again as many times as you like.</p>
  </section>
  <section>
    <h3>PHP</h3>
    <p>You use SQL to shoot yourself in the foot and send back an HTML file describing the results. Someone does some SQL injection and drops your bullets.</p>
  </section>
  <section>
    <h3>Windows</h3>
    <p>You load a Unicode gun with ANSI bullets and when you pull the trigger your foot is transformed into Asian gibberish.</p>
  </section>
  <section>
    <h3>Unix</h3>
    <p>You don’t have permission to access the foot or the gun.</p>
  </section>
  <section>
    <h3>Internet</h3>
    <p>You ask to be shot in the foot and are presented with 6.4 million choices for guns and bullets. By the time you finally make your decision, the request has timed out.</p>
  </section>
</main>
