<?xsite-params title="HTTP APIs" keywords="Opinion, HTTP, REST, SOAP, Apache Thrift, RPC, Remote Procedure Call, API, APIs, Application Programming Interfaces"?>
<main xmlns="http://www.w3.org/1999/xhtml">
  <h2>HTTP APIs</h2>
  <p>Many recent API styles (REST, SOAP) are built on HTTP. The justification for this is that it traverses proxies and firewalls and provides pre-built security functionality.</p>
  <p>In my opinion, this is a bad idea. HTTP is not an RPC protocol or any other kind of API protocol; it's a document retrieval protocol, and has very limited security support. Standard well-supported RPC protocols do exist (think Apache Thrift), and they provide security at or above the level that HTTP does. In addition, proxy traversal is not always good; some proxies modify requests and responses. In some cases it might be better for a request to fail than to be delivered in a potentially-corrupted state. Using a proper RPC protocol provides several advantages over HTTP:</p>
  <ul>
    <li>Nicer client-side API for less work.</li>
    <li>Makes proper firewalling easier by separating different applications.</li>
    <li>Allows better definition of the API.</li>
    <li>Allows security to be implemented without needing X.509 certificates.</li>
    <li>Doesn't misuse a protocol.</li>
  </ul>
  <p>In summary, using HTTP as an API transport is misuse of a protocol where better alternatives are available and causes numerous problems. Using a proper RPC protocol like Thrift provides a number of advantages.</p>
</main>
