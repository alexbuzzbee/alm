<?xml version="1.0" encoding="UTF-8"?>
<?xsite-params title="XSFI memory-mapped boot device annex" path="/projects/xsfi/memboot"?>
<main xmlns="http://www.w3.org/1999/xhtml">
  <h1>Extremely Simple Firmware Interface: Memory-Mapped Boot Device Annex</h1>
  <p><i>Draft 2, by <a href="https://www.alm.website/">Alex Martin</a> and <a href="https://drewdevault.com/">Drew DeVault</a></i></p>
  <p>Licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a>.</p>
  <p>The key words <strong>must</strong>, <strong>must not</strong>, <strong>required</strong>, <strong>shall</strong>, <strong>shall not</strong>, <strong>should</strong>, <strong>should not</strong>, <strong>recommended</strong>, <strong>not recommended</strong>, <strong>may</strong>, and <strong>optional</strong> in this document are to be interpreted as described in <a href="https://tools.ietf.org/html/bcp14">BCP 14</a> when, and only when, they appear in all bold-face, as shown here.</p>
  <section>
    <h1>Abstract</h1>
    <p>This document is an annex to the <a href=".">Extremely Simple Firmware Interface</a> that allows for booting from a memory-mapped device.</p>
  </section>
  <section>
    <h1>Section 1: Boot Tag</h1>
    <p>The Boot Tag type byte <code>03</code> is assigned for a Boot Tag with the following contents on any architecture:</p>
    <p>Table 1: Memory-Mapped Boot Device Boot Tag format</p>
    <table>
      <thead>
	<tr>
	  <th>Offset (bytes)</th>
	  <th>Size (bytes)</th>
	  <th>Format</th>
	  <th>Meaning</th>
	</tr>
      </thead>
      <tbody>
	<tr>
	  <td>0</td>
	  <td>(pointer size)</td>
	  <td>Pointer</td>
	  <td>Address of beginning of memory mapping</td>
	</tr>
	<tr>
	  <td>1*pointer size</td>
	  <td>(pointer size)</td>
	  <td>Native-endianness unsigned integer</td>
	  <td>Size of memory mapping</td>
	</tr>
	<tr>
	  <td>2*pointer size</td>
	  <td>4</td>
	  <td>Native-endianness unsigned integer</td>
	  <td>Size of logical block or zero</td>
	</tr>
      </tbody>
    </table>
    <p>The tag’s contents <strong>shall</strong> be filled by the Boot Firmware with appropriate values if the Boot Device is memory-mapped, or left as-is otherwise. Only one such tag may be present.</p>
  </section>
</main>
